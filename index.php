<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>D_LAN Portail</title>
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&display=swap" rel="stylesheet">
    <link rel="icon" type="image/png" href="assets/img/logo2.png">
</head>
<body>

    <nav>
        <a href="../index.html"><img src="assets/img/logoFullWide.png" alt="Logo D_LAN" class="logoFull"></a>
    </nav>
    <div class="underNav"></div>

    <div class="container">
        <div class="gamingGlowButtonContainer">
            <a href="./ticketFillerModule/ticketFillerMainView.html">
                <button class="gamingGlowButton">
                    <span>TicketFiller</span>
                </button>
            </a>

            <a href="./scriptCommand/scriptCommand.html">
                <button class="gamingGlowButton">
                    <span>scriptCommand</span>
                </button>
            </a>

            <a href="./cardAssort/cardAssort.html">
                <button class="gamingGlowButton">
                    <span>cardAssort</span>
                </button>
            </a>
            
            <a href="./notes/notes.html">
                <button class="gamingGlowButton">
                    <span>Notes</span>
                </button>
            </a>
            <a href="./creaSite/creaSite.html">
                <button class="gamingGlowButton">
                    <span>créaSite</span>
                </button>
            </a>
            <a href="./configGenerator/configGenerator.html">
                <button class="gamingGlowButton">
                    <span>configGenerator</span>
                </button>
            </a>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    TEST ZONE
                </div>
                <div class="col-12">
                    <h1>Copy in Clipboard with JavaScript</h1>

                    <textarea id="to-copy" spellcheck="false">Write something and copy it into your clipboard by clicking in the button below.</textarea>

                    <button id="copy" type="button">Copy in clipboard<span class="copiedtext" aria-hidden="true">Copied</span></button>

                    <textarea id="cleared" placeholder="Paste your copied content here. Just to test…"></textarea>

                    <p>Works on Firefox 41+, Chrome 42+, IE9+, Opera 29+, Safari 10+</p>
                </div>
            </div> 
        </div>
    </div>

    <script type="text/javascript" src="./assets/js/bootstrap.bundle.js"></script>

    <script>
        var toCopy  = document.getElementById( 'to-copy' ),
        btnCopy = document.getElementById( 'copy' ),
        paste   = document.getElementById( 'cleared' );

        btnCopy.addEventListener( 'click', function(){
        toCopy.select();
        paste.value = '';
        
        if ( document.execCommand( 'copy' ) ) {
            btnCopy.classList.add( 'copied' );
            paste.focus();
            
            var temp = setInterval( function(){
                btnCopy.classList.remove( 'copied' );
                clearInterval(temp);
            }, 600 );
            
        } else {
            console.info( 'document.execCommand went wrong…' )
        }
        
        return false;
    } );
    </script>
</body>
</html>