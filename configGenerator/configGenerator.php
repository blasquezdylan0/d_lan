<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>D_LAN Portail</title>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&display=swap" rel="stylesheet">
    <link rel="icon" type="image/png" href="../assets/img/logo2.png">
</head>
<body>

    <nav>
        <a href="../index.html"><img src="../assets/img/logoFullWide.png" alt="Logo D_LAN" class="logoFull"></a>
    </nav>
    <div class="underNav"></div>

    <div class="container">
        <?php echo "<h1>TEST</h1>" ?>
    </div>

    <script type="text/javascript" src="../assets/js/bootstrap.bundle.js"></script>
    <script type="text/javascript" src="../assets/js/configGenerator.js"></script>
</body>
</html>