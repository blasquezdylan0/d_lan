i = 1;
btnSubmitFluxPaloAlto.addEventListener("click", createResultFluxPaloAlto);
btnSubmitFluxMalware.addEventListener("click", createResultFluxMalware);
btnSubmitEquipment.addEventListener("click", createResultEquipment);
btnSubmitBlockedUrl.addEventListener("click", createResultBlockedUrl);

function createResultFluxPaloAlto() {
    const userAD = document.getElementById("userAD").value;
    const application = document.getElementById("application").value;
    var dateStart = document.getElementById("date-start").value;
    var dateEnd = document.getElementById("date-end").value;
    const timeStart = document.getElementById("time-start").value;
    const timeEnd = document.getElementById("time-end").value;
    
    const result = document.getElementById("result");

    dateStart = dateStart.replace(/-/g, "/");
    dateEnd = dateEnd.replace(/-/g, "/");

    result.textContent = "Les flux pour l'application " + 
        application + 
        " sont ouverts pour l'utilisateur " + 
        userAD + 
        " du " +
        dateStart +
        " au " +
        dateEnd +
        ", de " +
        timeStart +
        " à " +
        timeEnd;           
  }

  function createResultFluxMalware() {
    const userAD = document.getElementById("userADFluxMalware").value;

    const result = document.getElementById("resultFluxMalware");

    result.textContent = "Pas de flux malware pour l'utilisateur " +  
        userAD
  }

  function createResultEquipment() {
    const nameEquipement = document.getElementById("nameEquipment").value;
    const typeEquipment = document.getElementById("typeEquipment").value;

    const result = document.getElementById("resultEquipment");

    result.textContent = typeEquipment + " " + nameEquipement + " à bien été préparé et configuré et est disponible au bureau S1A04.";
    
  }

  function createResultBlockedUrl() {
    const URL = document.getElementById("blockedUrl").value;

    const result = document.getElementById("resultBlockedUrl");

    result.textContent = "L'URL suivante a été bloquée :  " +  
    URL
  }