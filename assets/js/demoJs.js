//GENERATE RANDOM PASSWORD

randomGenStringInput = document.getElementById("randomGenStringInput");
// randomGenStringInput = $("#randomGenStringInput");
randomGenStringButton = document.getElementById("randomGenStringButton");

allCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-*/@!:/;.,?"

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

function randomGenString(){
    strOut = "";

    for(let i = 0; i < 8; i++){
        ncar = getRandomInt(allCharacters.length);
        strOut = strOut + allCharacters.charAt(ncar);
    }

    console.log(strOut);
    randomGenStringInput.value = strOut;
}

//INSPECT JS OBJECT

const car = {
    color: 'black',
    manufacturer: 'Ford',
    model: 'Fiesta'
  }

const inspect = obj => {
    for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            console.log(`${prop}: ${obj[prop]}`)
        }
    }
  }

function inspectJsObject(){

    var css = 'font-size: 2em; background: #222; color: #bada55'

    console.log("%c ♠️ First Way : JSON.stringify",css);
    console.log(JSON.stringify(car, null, 2));
     
    console.log("%c ♠️ Second Way : Iterate the properties using a loop", css);
    inspect(car)

}