btnGenerateIVIT.addEventListener("click", generateIVIT);

function generateIVIT(){
  const DSIT = document.getElementById("DSIT").value;
  const nameSite = document.getElementById("nameSite").value;

  const LANIV = document.getElementById("LANIV");
  const LANIT = document.getElementById("LANIT");

  const TUNNELIV = document.getElementById("TUNNELIV");
  const TUNNELIT = document.getElementById("TUNNELIT");

  const PUBLICIV = document.getElementById("PUBLICIV");
  const PUBLICIT = document.getElementById("PUBLICIT");

  const ICOHD13IV = document.getElementById("ICOHD13IV");
  const ICOHD13IT = document.getElementById("ICOHD13IT");

  const ICOARENCIV = document.getElementById("ICOARENCIV");
  const ICOARENCIT = document.getElementById("ICOARENCIT");

  const TOIPIV = document.getElementById("TOIPIV");
  const TOIPIT = document.getElementById("TOIPIT");

  const IPPHONEIV = document.getElementById("IPPHONEIV");
  const IPPHONEIT = document.getElementById("IPPHONEIT");

  const VLAN10IV = document.getElementById("VLAN10IV");
  const VLAN10IT = document.getElementById("VLAN10IT");

 	
  LANIV.textContent = "IV - " + DSIT + " - " + nameSite + " - LAN";
  LANIT.textContent = "IT - " + DSIT + " - " + nameSite + " - LAN";

  TUNNELIV.textContent = "IV - " + DSIT + " - " + nameSite + " - TUNNEL";
  TUNNELIT.textContent = "IT - " + DSIT + " - " + nameSite + " - TUNNEL";$

  PUBLICIV.textContent = "IV - " + DSIT + " - " + nameSite + " - PUBLIC";$
  PUBLICIT.textContent = "IT - " + DSIT + " - " + nameSite + " - PUBLIC";$

  ICOHD13IV.textContent = "IV - " + DSIT + " - " + nameSite + " - ICO TUNNEL SSG5 PA3260 HD13";
  ICOHD13IT.textContent = "IT - " + DSIT + " - " + nameSite + " - ICO TUNNEL SSG5 PA3260 HD13";$

  ICOARENCIV.textContent = "IV - " + DSIT + " - " + nameSite + " - ICO TUNNEL SSG5 PA3260 ARENC";$
  ICOARENCIT.textContent = "IT - " + DSIT + " - " + nameSite + " - ICO TUNNEL SSG5 PA3260 ARENC";$

  TOIPIV.textContent = "IV - " + DSIT + " - " + nameSite + " - TOIP";$
  TOIPIT.textContent = "IT - " + DSIT + " - " + nameSite + " - TOIP";$

  IPPHONEIV.textContent = "IV - " + DSIT + " - " + nameSite + " - IPPHONE";$
  IPPHONEIT.textContent = "IT - " + DSIT + " - " + nameSite + " - IPPHONE";$

  VLAN10IV.textContent = "IV - " + DSIT + " - " + nameSite + " - INTERCO VLAN 10";$
  VLAN10IT.textContent = "IT - " + DSIT + " - " + nameSite + " - INTERCO VLAN 10";$


}

function copyToClipboard(text) {
  var dummy = document.createElement("textarea");
  document.body.appendChild(dummy);
  dummy.value = text;
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);
}

function animCopy(comp){
  comp.classList.add( 'copied' );
  var temp = setInterval( function(){
    comp.classList.remove( 'copied' );
    clearInterval(temp);
  }, 300 );
}

//Copy LAN
function copyLANIV(){
  comp = document.all.LANIV
  element = document.all.LANIV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyLANIT(){
  comp = document.all.LANIT
  element = document.all.LANIT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
}

// Copy TUNNEL
function copyTUNNELIV(){
  comp = document.all.TUNNELIV
  element = document.all.TUNNELIV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyTUNNELIT(){
  comp = document.all.TUNNELIT
  element = document.all.TUNNELIT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
}

// Copy PUBLIC
function copyPUBLICIV(){
  comp = document.all.PUBLICIV
  element = document.all.PUBLICIV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyPUBLICIT(){
  comp = document.all.PUBLICIT
  element = document.all.PUBLICIT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
}

// Copy INTERCO HD13
function copyICOHD13IV(){
  comp = document.all.ICOHD13IV
  element = document.all.ICOHD13IV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyICOHD13IT(){
  comp = document.all.ICOHD13IT
  element = document.all.ICOHD13IT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
}

// Copy INTERCO ARENC
function copyICOARENCIV(){
  comp = document.all.ICOARENCIV
  element = document.all.ICOARENCIV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyICOARENCIT(){
  comp = document.all.ICOARENCIT
  element = document.all.ICOARENCIT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
}

// Copy INTERCO TOIP
function copyTOIPIV(){
  comp = document.all.TOIPIV
  element = document.all.TOIPIV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyTOIPIT(){
  comp = document.all.TOIPIT
  element = document.all.TOIPIT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
}

// Copy INTERCO IPPHONE
function copyIPPHONEIV(){
  comp = document.all.IPPHONEIV
  element = document.all.IPPHONEIV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyIPPHONEIT(){
  comp = document.all.IPPHONEIT
  element = document.all.IPPHONEIT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
  
}

// Copy INTERCO VLAN10
function copyVLAN10IV(){
  comp = document.all.VLAN10IV
  element = document.all.VLAN10IV.innerHTML
  copyToClipboard(element);
  animCopy(comp);
}

function copyVLAN10IT(){
  comp = document.all.VLAN10IT
  element = document.all.VLAN10IT.innerHTML;
  copyToClipboard(element);
  animCopy(comp);
}