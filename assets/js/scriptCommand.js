
var now = new Date();

var annee   = now.getFullYear();
var mois    = now.getMonth() + 1;
var jour    = now.getDate();

console.log(annee);
console.log(mois);
console.log(jour);

if(mois <=9 ) {
  mois = "0" + mois;
}

if(jour <=9 ) {
  jour = "0" + jour;
}


document.getElementById("scriptStep1").textContent = "zcat messages-" + annee + mois + jour + ".gz | grep \"Access-Profile_Portailvpn-Network\" | egrep \"apmd|tmm1\" > /var/tmp/apmd_vpn.log"
document.getElementById("scriptStep2").textContent = "zcat messages-" + annee + mois + jour + ".gz | grep \"Access-Profile_Portailweb-ClientLess\" | egrep \"apmd|tmm1\" > /var/tmp/apmd_vpn.log"
document.getElementById("scriptStep3").textContent = "zcat messages-" + annee + mois + jour + ".gz | grep hd7.cg13.fr | grep \"Context\" | cut -d ' ' -f 1-17 | grep -oE '[^ ]+$' | sort –-unique"

var toCopy  = document.getElementById( 'scriptStep1' ),
    btnCopy = document.getElementById( 'copy' ),
    paste   = document.getElementById( 'cleared' );

btnCopy.addEventListener( 'click', function(){
  toCopy.select();
  paste.value = '';
  
  if ( document.execCommand( 'copy' ) ) {
    paste.focus();
 
  } else {
    console.info( 'document.execCommand went wrong…' )
  }
  
  return false;
} );
  